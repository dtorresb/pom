package com.choucair.formacion.pageobjects;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

import com.ibm.icu.text.DecimalFormat;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas/productos-servicios/creditos/consumo/libre-inversion/simulador-credito-consumo")
public class simulacion extends PageObject {
	// Aqui se deben de realizar las validaciones de todas los campos del formulario

	// Campo de selección multiple: qué se desea simular
	@FindBy(xpath = "//*[@id=\"sim-detail\"]/form/div[2]/select")
	public WebElementFacade txtSimula;

	// Fecha de nacimiento
	@FindBy(name = "dateFechaNacimiento")
	public WebElementFacade txtBornDate;

	// Campo de selección multiple tasa del prestamo
	@FindBy(xpath = "//*[@id=\'sim-detail\']/form/div[4]/select")
	public WebElementFacade txtTasaPrestamo;

	// Campo de selección multiple: tipo de credito que se desea adquirir
	@FindBy(xpath = "//*[@id=\'sim-detail\']/form/div[5]/div[1]/select")
	public WebElementFacade txtTipoCredito;

	// Plazo del credito
	@FindBy(xpath = "//*[@id=\'sim-detail\']/form/div[7]/div[1]/input")
	public WebElementFacade txtPlazoCredito;

	// Monto del credito
	@FindBy(xpath = "//*[@id=\'sim-detail\']/form/div[7]/div[2]/input")
	public WebElementFacade txtMontoCredito;

	// Boton simulación
	@FindBy(xpath = "//*[@id=\'sim-detail\']/form/div[8]/button")
	public WebElementFacade botonSimular;

	// Valor del credito de Bancolombia
	@FindBy(xpath = "//*[@id=\'sim-results\']/div[1]/table/tbody/tr[3]/td[3]")
	public WebElementFacade creditoBancolombia;

	// Definición de funciones

	public String getValorCredito() {
		//double valor=0;
		//valor = Double.parseDouble(creditoBancolombia.getText());
		//return valor;
		return creditoBancolombia.getText();
	}

	// Presionar boton simular
	public void ingresarDatos() {
		botonSimular.click();
	}
	
	public void verificarCuotas(double tasa_interes, double numero_cuotas, double valor_credito){
		double cuota = 0;
		double i = Math.pow((1 + tasa_interes), numero_cuotas);
		cuota = valor_credito * ((tasa_interes * i) / (i - 1));
		DecimalFormat formateador = new DecimalFormat("###,###.##");
		String verificacion = "$" + formateador.format(cuota);
		String credito_arrojado = getValorCredito();
		credito_arrojado = credito_arrojado.replace('.', '*');
		

		credito_arrojado = credito_arrojado.replace(',', '.');
		credito_arrojado = credito_arrojado.replace('*', ',');
		
		assertThat(verificacion, containsString(credito_arrojado));
	}

	// ¿Qué se desea simular?
	public void simular(String DatoPrueba) {
		txtSimula.sendKeys(DatoPrueba);
	}

	// Fecha de nacimiento
	public void fechaNacimiento(String DatoPrueba) {
		txtBornDate.clear();
		txtBornDate.sendKeys(DatoPrueba);
	}

	// Tasa de pestamo
	public void tasaPrestamo(String DatoPrueba) {
		txtTasaPrestamo.sendKeys(DatoPrueba);
	}

	// Tipo de credito
	public void tipoCredito(String DatoPrueba) {
		txtTipoCredito.sendKeys(DatoPrueba);
	}

	// Plazo credito
	public void plazoCredito(String DatoPrueba) {
		txtPlazoCredito.click();
		txtPlazoCredito.clear();
		txtPlazoCredito.sendKeys(DatoPrueba);
	}

	// Monto credito
	public void montoCredito(String DatoPrueba) {
		txtMontoCredito.click();
		txtMontoCredito.clear();
		txtMontoCredito.sendKeys(DatoPrueba);
	}
}
