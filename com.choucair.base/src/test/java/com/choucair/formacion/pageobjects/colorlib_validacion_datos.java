package com.choucair.formacion.pageobjects;

import static org.junit.Assert.assertThat;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


//import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

//import static org.junit.Assert.*;
//import static org.junit.Assert.assertEquals;

public class colorlib_validacion_datos extends PageObject{
	//Required
	@FindBy(id="req")
	public WebElementFacade txtRequired;
	
	//Sport
	@FindBy(id="sport")
	public WebElementFacade txtSport;
	
	//Sport
	@FindBy(id="sport2")
	public WebElementFacade txtSport2;

		
	//URL
	@FindBy(id="url1")
	public WebElementFacade txturl;
	
	//Email
	@FindBy(id="email1")
	public WebElementFacade txtEmail;
	
	//Password
	@FindBy(id="pass1")
	public WebElementFacade txtPassword;
	
	//Password2
	@FindBy(id="pass2")
	public WebElementFacade txtPassword2;
	
	//Min Size
	@FindBy(id="minsize1")
	public WebElementFacade txtMinSize;
	
	//Max Size
	@FindBy(id="maxsize1")
	public WebElementFacade txtMaxSize;
	
	//Number
	@FindBy(id="number2")
	public WebElementFacade txtNumber;
	
	//IP
	@FindBy(id="ip")
	public WebElementFacade txtIP;
	
	//Date
	@FindBy(id="date3")
	public WebElementFacade txtDate;
	
	//Date Earlier
	@FindBy(id="past")
	public WebElementFacade txtDateEarlier;
	
	//Button Validate
	@FindBy(xpath="//*[@id=\'popup-validation\']/div[14]/input")
	public WebElementFacade buttonValidate;
	
	//Blobo informativo
	@FindBy(xpath="(//DIV[@class='formErrorContent'])[1]")
	public WebElementFacade globo_informativo;
	
	public void Required(String DatoPrueba) {
		txtRequired.click();
		txtRequired.clear();
		txtRequired.sendKeys(DatoPrueba);
	}
	
	public void Sport(String DatoPrueba) {
		txtSport.click();
		txtSport.selectByVisibleText(DatoPrueba);
	}
	
	public void Sport2(String DatoPrueba) {
		//txtSport2.click();
		txtSport2.selectByVisibleText(DatoPrueba);
	}
	
	public void URL(String DatoPrueba) {
		txturl.click();
		txturl.clear();
		txturl.sendKeys(DatoPrueba);
	}
	
	public void Email(String DatoPrueba) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(DatoPrueba);
	}
	
	public void Password(String DatoPrueba) {
		txtPassword.click();
		txtPassword.clear();
		txtPassword.sendKeys(DatoPrueba);
	}
	
	public void Password2(String DatoPrueba) {
		txtPassword2.click();
		txtPassword2.clear();
		txtPassword2.sendKeys(DatoPrueba);
	}
	
	public void MinSize(String DatoPrueba) {
		txtMinSize.click();
		txtMinSize.clear();
		txtMinSize.sendKeys(DatoPrueba);
	}
	
	public void MaxSize(String DatoPrueba) {
		txtMaxSize.click();
		txtMaxSize.clear();
		txtMaxSize.sendKeys(DatoPrueba);
	}
	
	public void Number(String DatoPrueba) {
		txtNumber.click();
		txtNumber.clear();
		txtNumber.sendKeys(DatoPrueba);
	}
	
	public void IP(String DatoPrueba) {
		txtIP.click();
		txtIP.clear();
		txtIP.sendKeys(DatoPrueba);
	}
	
	public void Date(String DatoPrueba) {
		txtDate.click();
		txtDate.clear();
		txtDate.sendKeys(DatoPrueba);
	}
	
	public void DateEarlier(String DatoPrueba) {
		txtDateEarlier.click();
		txtDateEarlier.clear();
		txtDateEarlier.sendKeys(DatoPrueba);
	}
	
	public void valida() {
		buttonValidate.click();
	}
	
	//Verifica si no se encuentran errores al momento de digitar los datos.
	public void form_sin_errores() {
		assertThat(globo_informativo.isCurrentlyVisible(), is(false));
	}

	
	public void form_con_errores() {
		assertThat(globo_informativo.isCurrentlyVisible(), is(true));
	}
	
	
	
	
}
