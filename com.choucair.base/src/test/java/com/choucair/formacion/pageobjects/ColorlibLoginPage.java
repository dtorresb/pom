package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class ColorlibLoginPage extends PageObject{
	//User
	@FindBy(xpath="//*[@id=\'login\']/form/input[1]")
	public WebElementFacade txtUsername;
	
	//Password
	@FindBy(xpath="//*[@id=\'login\']/form/input[2]")
	public WebElementFacade txtPassword;
	
	//Log in
	@FindBy(xpath="//*[@id=\'login\']/form/button")
	public WebElementFacade btnSigIn;
	
	//Log in button
	@FindBy(xpath="//*[@id=\'bootstrap-admin-template\']")
	public WebElementFacade lblHomePpal;
	
	public void ingresarDatos(String Usuario, String Contrasena) {
		txtUsername.sendKeys(Usuario);
		txtPassword.sendKeys(Contrasena);
		btnSigIn.click();
	}
	
	public void verificarHome() {
		String labelv = "Bootstrap-Admin-Template";
		String strMensaje = lblHomePpal.getText();
		assertThat(strMensaje, containsString(labelv));
	}
}
