package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class canon extends PageObject {
	// Productos y servicios
	@FindBy(xpath = "//*[@id=\'main-menu\']/div[2]/ul[1]/li[3]/a")
	public WebElementFacade productos_y_servicios;

	// Leasing
	@FindBy(xpath = "//*[@id=\'productosPersonas\']/div/div[1]/div/div/div[2]/div/a")
	public WebElementFacade leasing;

	// Leasing habitacional
	@FindBy(xpath = "//*[@id=\'category-detail\']/div/div/div[2]/div/div[2]/h2/a")
	public WebElementFacade leasing_habitacional;

	// Simular canon
	@FindBy(xpath = "//*[@id=\'main-content\']/div[4]/div/div/div/div/div[1]/div/div/div[1]/a")
	public WebElementFacade simular_canon_constant;

	// valor activo
	@FindBy(name = "valorActivo")
	public WebElementFacade valor_activo;

	// valor activo
	@FindBy(name = "plazo")
	public WebElementFacade plazo;

	// Porcentaje opción de compra
	@FindBy(name = "opcionCompra")
	public WebElementFacade porcentaje_opcion_compra;

	// Tipo de tasa
	@FindBy(name = "comboTipoTasa")
	public WebElementFacade tipo_tasa;

	// Boton simular
	@FindBy(name = "simular")
	public WebElementFacade boton_similar;

	// Label despues de simular
	@FindBy(xpath = "//*[@id=\'resultado\']/div/table/tbody/tr[1]/td[1]")
	public WebElementFacade txt_valor_canon;
	

	public void rutaAPagina() {
		productos_y_servicios.click();
		leasing.click();
		leasing_habitacional.click();
		simular_canon_constant.click();
	}

	public void valor_activo_completar(String DatoPrueba) {
		valor_activo.clear();
		valor_activo.sendKeys(DatoPrueba);
	}

	public void plazo_completar(String DatoPrueba) {
		plazo.clear();
		plazo.sendKeys(DatoPrueba);
	}

	public void porcentaje_opcion_compra_completar(String DatoPrueba) {
		porcentaje_opcion_compra.clear();
		porcentaje_opcion_compra.sendKeys(DatoPrueba);
	}

	public void tipo_tasa_completar(String DatoPrueba) {
		tipo_tasa.sendKeys(DatoPrueba);
	}

	public void boton_similar_presionar() {
		boton_similar.click();
	}
	
	public void verificarSimulacion() {
		String labelv = "Valor canon mensual";
		String strMensaje = txt_valor_canon.getText();
		assertThat(strMensaje, containsString(labelv));
	}

}
