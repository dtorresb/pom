package com.choucair.formacion.pageobjects;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class colorlib_bloc_validation extends PageObject {
	// Required
	@FindBy(id = "required2")
	public WebElementFacade txtRequired2;

	// Email
	@FindBy(id = "email2")
	public WebElementFacade txtEmail2;

	// Password
	@FindBy(id = "password2")
	public WebElementFacade txpassword2;

	// Confirmar password
	@FindBy(id = "confirm_password2")
	public WebElementFacade txtconfirm_password2;

	// date
	@FindBy(id = "date2")
	public WebElementFacade txtdate2;

	// URL
	@FindBy(id = "url2")
	public WebElementFacade txturl2;

	// Digits only
	@FindBy(id = "digits")
	public WebElementFacade txtdigits;

	// Range
	@FindBy(id = "range")
	public WebElementFacade txtrange;

	// Agree Policy
	@FindBy(id = "agree2")
	public WebElementFacade txtagree2;

	// Button Validate
	@FindBy(xpath = "//*[@id=\'block-validate\']/div[10]/input")
	public WebElementFacade buttonValidate;
	
	// Mistakes
	@FindBy(className = "help-block")
	public WebElementFacade m_mistakes;

	public void Required2(String DatoPrueba) {
		txtRequired2.click();
		txtRequired2.clear();
		txtRequired2.sendKeys(DatoPrueba);
	}

	public void Email2(String DatoPrueba) {
		txtEmail2.click();
		txtEmail2.clear();
		txtEmail2.sendKeys(DatoPrueba);
	}

	public void password2(String DatoPrueba) {
		txpassword2.click();
		txpassword2.clear();
		txpassword2.sendKeys(DatoPrueba);
	}

	public void confirm_password2(String DatoPrueba) {
		txtconfirm_password2.click();
		txtconfirm_password2.clear();
		txtconfirm_password2.sendKeys(DatoPrueba);
	}

	public void date2(String DatoPrueba) {
		txtdate2.click();
		txtdate2.sendKeys(DatoPrueba);
	}

	public void url2(String DatoPrueba) {
		txturl2.click();
		txturl2.clear();
		txturl2.sendKeys(DatoPrueba);
	}

	public void digits(String DatoPrueba) {
		txtdigits.click();
		txtdigits.clear();
		txtdigits.sendKeys(DatoPrueba);
	}

	public void range(String DatoPrueba) {
		txtrange.click();
		txtrange.clear();
		txtrange.sendKeys(DatoPrueba);
	}

	public void agree2() {
		txtagree2.click();
	}



	public void validacion() {
		buttonValidate.click();
	}

	// Verifica si no se encuentran errores al momento de digitar los datos.
	public void block_con_errores() {
		assertThat(m_mistakes.isCurrentlyVisible(), is(true));
	}
	
	public void block_sin_errores() {
		assertThat(m_mistakes.isCurrentlyVisible(), is(false));
	}

}
