package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ColorlibMenuPage extends PageObject{
	//Menu form
	//Find principal menu
	@FindBy(xpath="//*[@id=\'menu\']/li[6]/a")
	public WebElementFacade PrincipalMenu;
	
	//Find sub menu
	@FindBy(xpath="//*[@id=\'menu\']/li[6]/ul/li[2]/a")
	public WebElementFacade SubMenu;
	
	//Find label form validation
	@FindBy(xpath="//*[@id=\'content\']/div/div/div[1]/div/div/header/h5")
	public WebElementFacade txtLabel;
	
	//Find label block validation
	@FindBy(xpath="//*[@id=\'content\']/div/div/div[2]/div/div/header/h5")
	public WebElementFacade txtLabel2;
	
	public void MenuValidation(String valor, int i) {
		PrincipalMenu.click();
		SubMenu.click();
		if(i==1) { //Verificacion form validation
			String strLabel = txtLabel.getText();
			assertThat(strLabel, containsString(valor));
		}else if (i==2) {	//Verificacion block validation
			String strLabel = txtLabel2.getText();
			assertThat(strLabel, containsString(valor));
		}
	}
	
	
}
