package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.zoho.com/es-xl/crm/")
public class cmrObjects extends PageObject {
	// Full name
	@FindBy(xpath = "//*[@id=\'namefield\']")
	public WebElementFacade txtFullName;

	// Email
	@FindBy(xpath = "//*[@id=\'email\']")
	public WebElementFacade txtEmailBox;

	// Password
	@FindBy(xpath = "//*[@id=\'dialogRegister\']/div[3]/div/input")
	public WebElementFacade txtPassBox;

	// Country code
	@FindBy(xpath = "//*[@id=\'country_code\']")
	public WebElementFacade txtCountryCode;

	// Mobile
	@FindBy(xpath = "//*[@id=\'mobile\']")
	public WebElementFacade txtMobile;

	// Country
	@FindBy(xpath = "//*[@id=\'country\']")
	public WebElementFacade txtcountry;

	// CheckBox
	@FindBy(xpath = "//*[@id=\'dialogRegister\']/div[7]/div[2]/label")
	public WebElementFacade txtcheckboxss;

	// Boton sign up
	@FindBy(xpath = "// *[@id=\'signupbtn\']")
	public WebElementFacade txtbotonsing;
	
	public void FullName(String DatoPrueba) {
		txtFullName.clear();
		txtFullName.sendKeys(DatoPrueba);
	}
	
	public void EmailBox(String DatoPrueba) {
		txtEmailBox.clear();
		txtEmailBox.sendKeys(DatoPrueba);
	}
	
	public void PassBox(String DatoPrueba) {
		txtPassBox.clear();
		txtPassBox.sendKeys(DatoPrueba);
	}
	
	public void CountryCode(String DatoPrueba) {
		txtCountryCode.sendKeys(DatoPrueba);
	}
	
	public void Mobile(String DatoPrueba) {
		txtMobile.clear();
		txtMobile.sendKeys(DatoPrueba);
	}

	public void country(String DatoPrueba) {
		txtcountry.sendKeys(DatoPrueba);
	}

	public void checkbox3() {
		txtcheckboxss.click();
	}
	
	public void singup() {
		txtbotonsing.click();
	}
	

}
