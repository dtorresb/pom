/**
 * 
 */
/**
 * @author dtorresb
 *
 */
package com.choucair.formacion.definition;

import com.choucair.formacion.steps.ValidationSteps;
import com.choucair.formacion.steps.validaciondedatos;
//import com.ibm.icu.impl.LocaleDisplayNamesImpl.DataTable;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.DataTable;
import net.thucydides.core.annotations.Step;
//import groovyjarjarantlr.collections.List;
import net.thucydides.core.annotations.Steps;

import java.util.ArrayList;
import java.util.List;

public class PopupValitadion {
	
	@Steps
	ValidationSteps ValidationStepss;
	@Steps
	validaciondedatos validaciondedatoss;
		
	@Given("^Autenticación en colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autentico_usuario_clave(String Usuario, String Clave) {
		ValidationStepss.autentico_usuario_clave(Usuario, Clave);
	}
	
	@Given("^Ingreso a la funcionalidad Forms Validation$")
	public void funcionalidad_forms_validation() {
		ValidationStepss.ColorlibMenuPage("Popup Validation",1);
	}
	
	@Given("^Ingreso a la funcionalidad Block Validation$")
	public void funcionalidad_block_validation() {
		ValidationStepss.ColorlibMenuPage("Block Validation",2);
	}
	
	@When("^Diligencio Formulario Popup Validation$")
	public void diligencio_formulario_forms_validation(DataTable dataform){
		//validaciondedatoss.escribirrequire();
		List<List<String>> data = dataform.raw();
		for(int i=0; i<data.size(); i++) {
			validaciondedatoss.diligenciar_datos_tabla(data, i);
			try {
				Thread.sleep(5000);
			}catch (InterruptedException e) {
				// TODO: handle exception
			}
		}
	}
	
	@When("^Diligencio formulario block validation$")
	public void diligencio_formulario_block_validation(DataTable dataform){
		List<List<String>> data = dataform.raw();
		for(int i=0; i<data.size(); i++) {
			validaciondedatoss.diligenciar_datos_tabla_block(data, i);
			try {
				Thread.sleep(5000);
			}catch (InterruptedException e) {
				// TODO: handle exception
			}
		}
	}
		
	@Then("^Verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso() {
		validaciondedatoss.verificar_sin_errores();
	}
	
	@Then("^Verificar que se presente globo informativo$")
	public void verificar_globo_informativo() {
		validaciondedatoss.form_con_errores();
	}
	
	@Then("^Verificar mensajes de error$")
	public void verificar_mensajes_error() {
		validaciondedatoss.block_con_errores();
	}
	
	@Then("^Verificar sin mensajes de mensajes de error$")
	public void verificar_mensajes_sin_error() {
		validaciondedatoss.block_sin_errores();
	}
	
	
}