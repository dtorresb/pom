package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.cmrValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CMR_registro {
	
	@Steps
	cmrValidationSteps cmrValidationStepss;
	
	@Given("^el usuario quiere utilizar el CRM Zoho$")
	public void el_usuario_quiere_utilizar_el_CRM_Zoho() throws Throwable {
		cmrValidationStepss.ingresoAPaginaCmr();
		
	}

	@When("^realizo el registro exitoso$")
	public void realizo_el_registro_exitoso(DataTable dataform) throws Throwable {
		List<List<String>> data = dataform.raw();
		for (int i = 0; i < data.size(); i++) {
			cmrValidationStepss.diligenciar_formulario_cmr(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO: handle exception
			}
		}
	}

	@Then("^s Verifico el acceso a la aplicacion$")
	public void s_Verifico_el_acceso_a_la_aplicacion() throws Throwable {
	}
}
