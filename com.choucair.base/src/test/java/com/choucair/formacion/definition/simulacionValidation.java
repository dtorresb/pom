package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.ValidationSteps;
import com.choucair.formacion.steps.simulationSteps;
import com.choucair.formacion.steps.validaciondedatos;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class simulacionValidation {
	@Steps
	simulationSteps simulacion;

	@Given("^ingreso a la página de prueba$")
	public void autentico_usuario_clave() {
		simulacion.ingresoPagina();
	}

	@When("^Digito los campos del formulario$")
	public void diligencio_formulario(DataTable dataform) {
		List<List<String>> data = dataform.raw();
		for (int i = 0; i < data.size(); i++) {
			simulacion.diligenciar_campos(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO: handle exception
			}
		}
	}
	
	@Then("^Verifico el calculo de la cuota$")
	public void verificacionIngreso() {
	}
	
}
