package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.ValidationSteps;
import com.choucair.formacion.steps.canonValidationSteps;
import com.choucair.formacion.steps.validaciondedatos;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class canonSimulation {
	@Steps
	canonValidationSteps canonValidationStepss;
		
	@Given("^ingreso a la página dada$")
	public void autentico_usuario_clave() {
		canonValidationStepss.ingresoAPagina();
	}
	
	@When("^Digito los campos del formulario canon$")
	public void completar_datos(DataTable dataform) {
		List<List<String>> data = dataform.raw();
		for (int i = 0; i < data.size(); i++) {
			canonValidationStepss.diligenciar_formulario(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO: handle exception
			}
		}
	}
	
	@Then("^Verifico la generacion de loa simulacion$")
	public void verifico_la_generacion_de_loa_simulacion() throws Throwable {
		canonValidationStepss.validacionSimulacion();
	}

}
