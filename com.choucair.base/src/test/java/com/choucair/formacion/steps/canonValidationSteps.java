package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.canon;
import com.choucair.formacion.pageobjects.simulacion;

import ch.lambdaj.function.aggregate.Concat;
import net.thucydides.core.annotations.Step;


public class canonValidationSteps {
	
	canon canon_sim;

	@Step
	public void ingresoAPagina() {
		canon_sim.open();
		canon_sim.rutaAPagina();
	}
	
	@Step
	public void validacionSimulacion() {
		canon_sim.verificarSimulacion();
	}
	
	@Step
	public void diligenciar_formulario(List<List<String>> data, int id) {
		canon_sim.valor_activo_completar(data.get(id).get(0).trim());
		canon_sim.plazo_completar(data.get(id).get(1).trim());
		canon_sim.porcentaje_opcion_compra_completar(data.get(id).get(2).trim());
		canon_sim.tipo_tasa_completar(data.get(id).get(3).trim());
		canon_sim.boton_similar_presionar();
	}
	
}
