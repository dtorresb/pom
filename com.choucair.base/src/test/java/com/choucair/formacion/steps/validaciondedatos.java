package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.colorlib_bloc_validation;
import com.choucair.formacion.pageobjects.colorlib_validacion_datos;

//import groovyjarjarantlr.collections.List;
//import java.util.ArrayList;
import java.util.List;
import net.thucydides.core.annotations.Step;

public class validaciondedatos {
	
	colorlib_validacion_datos c_vali_datos;
	colorlib_bloc_validation c_vali_datos_block;
	
	@Step
	public void escribirrequire() {
		c_vali_datos.Required("dato");
	}
	
	@Step
	public void diligenciar_datos_tabla(List<List<String>> data, int id) {
		c_vali_datos.Required(data.get(id).get(0).trim());
		c_vali_datos.Sport(data.get(id).get(1).trim());
		c_vali_datos.Sport2(data.get(id).get(2).trim());
		c_vali_datos.URL(data.get(id).get(3).trim());
		c_vali_datos.Email(data.get(id).get(4).trim());
		c_vali_datos.Password(data.get(id).get(5).trim());
		c_vali_datos.Password2(data.get(id).get(6).trim());
		c_vali_datos.MinSize(data.get(id).get(7).trim());
		c_vali_datos.MaxSize(data.get(id).get(8).trim());
		c_vali_datos.Number(data.get(id).get(9).trim());
		c_vali_datos.IP(data.get(id).get(10).trim());
		c_vali_datos.Date(data.get(id).get(11).trim());
		c_vali_datos.DateEarlier(data.get(id).get(12).trim());
		c_vali_datos.valida();
	}
	
	@Step
	public void diligenciar_datos_tabla_block(List<List<String>> data, int id) {
		c_vali_datos_block.Required2(data.get(id).get(0).trim());
		c_vali_datos_block.Email2(data.get(id).get(1).trim());
		c_vali_datos_block.password2(data.get(id).get(2).trim());
		c_vali_datos_block.confirm_password2(data.get(id).get(3).trim());
		c_vali_datos_block.date2(data.get(id).get(4).trim());
		c_vali_datos_block.url2(data.get(id).get(5).trim());
		c_vali_datos_block.digits(data.get(id).get(6).trim());
		c_vali_datos_block.range(data.get(id).get(7).trim());
		c_vali_datos_block.agree2();
		c_vali_datos_block.validacion();
	}
	
	
	@Step
	public void verificar_sin_errores() {
		c_vali_datos.form_sin_errores();
	}
	
	@Step
	public void form_con_errores() {
		c_vali_datos.form_con_errores();
	}
	
	@Step
	public void block_con_errores() {
		c_vali_datos_block.block_con_errores();
	}
	
	@Step
	public void block_sin_errores() {
		c_vali_datos_block.block_sin_errores();
	}
	
	
}
