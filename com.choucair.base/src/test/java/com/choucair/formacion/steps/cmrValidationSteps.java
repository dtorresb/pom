package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.cmrObjects;

import net.thucydides.core.annotations.Step;

public class cmrValidationSteps {
	cmrObjects cmr;

	@Step
	public void ingresoAPaginaCmr() {
		cmr.open();
	}
	
	@Step
	public void diligenciar_formulario_cmr(List<List<String>> data, int id) {
		cmr.FullName(data.get(id).get(0).trim());
		cmr.EmailBox(data.get(id).get(1).trim());
		cmr.PassBox(data.get(id).get(2).trim());
		//cmr.CountryCode(data.get(id).get(3).trim());
		cmr.Mobile(data.get(id).get(4).trim());
		cmr.country(data.get(id).get(5).trim());
		cmr.checkbox3();
		cmr.singup();
	}
	
	
}
