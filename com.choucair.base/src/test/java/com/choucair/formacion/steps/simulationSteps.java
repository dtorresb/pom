package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.simulacion;
import com.ibm.icu.text.DecimalFormat;

import ch.lambdaj.function.aggregate.Concat;
import net.thucydides.core.annotations.Step;

public class simulationSteps {
	simulacion sim;

	@Step
	public void ingresoPagina() {
		sim.open();

	}

	@Step
	public void diligenciar_campos(List<List<String>> data, int id) {
		sim.simular(data.get(id).get(0).trim());
		sim.fechaNacimiento(data.get(id).get(1).trim());
		sim.tasaPrestamo(data.get(id).get(2).trim());
		sim.tipoCredito(data.get(id).get(3).trim());
		sim.plazoCredito(data.get(id).get(4).trim());
		sim.montoCredito(data.get(id).get(5).trim());
		sim.ingresarDatos();
		

		// Realizar calculo del valor de la cuota
		double tasa_interes = Double.parseDouble(data.get(id).get(6).trim());
		double numero_cuotas = Double.parseDouble(data.get(id).get(4).trim());
		double valor_credito = Double.parseDouble(data.get(id).get(5).trim());
		sim.verificarCuotas(tasa_interes, numero_cuotas, valor_credito);
		

		/*
		 * if(credito_arrojado == cuota) { return true; }else { return false; }
		 */

	}
}
