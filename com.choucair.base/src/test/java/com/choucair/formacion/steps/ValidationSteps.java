package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.ColorlibLoginPage;
import com.choucair.formacion.pageobjects.ColorlibMenuPage;

import net.thucydides.core.annotations.Step;

public class ValidationSteps {
	ColorlibLoginPage colorlibloginpage;
	ColorlibMenuPage colorlibMenuPage;
	
	@Step
	public void autentico_usuario_clave(String strUsuario, String strClave) {
		//Abrir el navegador con la url de pruebas
		colorlibloginpage.open();
		
		//Ingresar usuario, contraseña y clic en loguin
		colorlibloginpage.ingresarDatos(strUsuario, strClave);
		
		//Verificar la autenticación
		colorlibloginpage.verificarHome();
	}
	
	@Step
	public void ColorlibMenuPage(String valor, int i) {
		colorlibMenuPage.MenuValidation(valor, i);
	}
}
