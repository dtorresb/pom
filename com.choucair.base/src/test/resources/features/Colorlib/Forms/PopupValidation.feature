#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@Regresion
Feature: Formulario Popup Validation
	El usuario debe poder ingresar al formulario los campos requeridos.
	Cada campo del formulario es obligatorio,
	longitud y formato, el sistema debe presentar las validaciones respectivas
	para cada campo a través de un globo informativo.
	
	
@CasoExitoso
Scenario: Diligenciamiento exitoso del formulario Popuo Validation,
					No se presenta ningún mensaje de validación.
Given Autenticación en colorlib con usuario "demo" y clave "demo"
	And Ingreso a la funcionalidad Forms Validation
When Diligencio Formulario Popup Validation
	 | valor1 | Golf | Tennis | http://www.valor1.com | valor1@gmail.com | valor1 | valor1 | 123456 | 123456 | -99.99 | 200.200.5.4 | 2012-12-01 | 2012/09/12 |
Then Verifico ingreso exitoso

@CasoAlterno
Scenario: Diligenciamiento exitoso del formulario Popuo Validation,
					No se presenta ningún mensaje de validación.
Given Autenticación en colorlib con usuario "demo" y clave "demo"
	And Ingreso a la funcionalidad Forms Validation
When Diligencio Formulario Popup Validation
	 |  | Golf | Tennis | http://www.valor1.com | valor1@gmail.com | valor1 | valor1 | 123456 | 123456 | -99.99 | 200.200.5.4 | 2012-12-01 | 2012/09/12 |
	 | valor1 | Choose a sport | Tennis | http://www.valor1.com | valor1@gmail.com | valor1 | valor1 | 123456 | 123456 | -99.99 | 200.200.5.4 | 2012-12-01 | 2012/09/12 |
Then Verificar que se presente globo informativo

@casoFelizBlockValidation
Scenario: Diligenciamiento de formulario block validation
					No se presenta ningun mensaje de validación.
Given Autenticación en colorlib con usuario "demo" y clave "demo"
	And Ingreso a la funcionalidad Block Validation
When Diligencio formulario block validation
| valor1 | valor1@gmail.com | Tennis | Tennis | 04/04/2018 | http://www.valor1.com | 12 | 8 |
#Then Verificar sin mensajes de mensajes de error

@casoAlternoBlockValidation
Scenario: Diligenciamiento de formulario block validation
					No se presenta ningun mensaje de validación.
Given Autenticación en colorlib con usuario "demo" y clave "demo"
	And Ingreso a la funcionalidad Block Validation
When Diligencio formulario block validation
|  | valor1@gmail.com | Tennis |  | 04/04/2018 | http://www.valor1.com | 12 | 8 |
Then Verificar mensajes de error