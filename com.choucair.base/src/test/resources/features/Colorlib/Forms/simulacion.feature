@Regresion
Feature: Realizar el calculo de la cuota de un credito para una tasa de interes fija y de libre inversión
  El sistema calcula la cuota segun los datos ingresados

  @simulacionBancolombia
  Scenario: Diligenciamiento formulario simulacion de credito
    Given ingreso a la página de prueba
    When Digito los campos del formulario
      | Simula tu Cuota | 1995-07-08 | Tasa Fija | Crédito de Libre Inversión | 60 | 10000000 | 0.0135 | 
    Then Verifico el calculo de la cuota
    
   